//
//  MyMacros.h
//  FunnyTelco
//
//  Created by Mathias Hansen on 01/03/16.
//  Copyright © 2016 Mathias Hedemann Hansen. All rights reserved.
//

#ifndef MyMacros_h
#define MyMacros_h

/* If we have no server */
#define FAKE_SERVER YES

#endif /* MyMacros_h */
