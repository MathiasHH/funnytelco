//
//  LoginRules.m
//  FunnyTelco
//
//  Created by Mathias Hansen on 05/03/16.
//  Copyright © 2016 Mathias Hedemann Hansen. All rights reserved.
//

#import "LoginRules.h"

@implementation LoginRules

- (BOOL)isValidEmail:(NSString *)email
{
    if (![email length])
    {
        return NO;
    }

    NSRange entireRange = NSMakeRange(0, [email length]);
    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:NULL];
    NSArray *matches = [detector matchesInString:email options:0 range:entireRange];

    // should only a single match
    if ([matches count]!=1)
    {
        return NO;
    }

    NSTextCheckingResult *result = [matches firstObject];

    // result should be a link
    if (result.resultType != NSTextCheckingTypeLink)
    {
        return NO;
    }

    // result should be a recognized mail address
    if (![result.URL.scheme isEqualToString:@"mailto"])
    {
        return NO;
    }

    // match must be entire string
    if (!NSEqualRanges(result.range, entireRange))
    {
        return NO;
    }

    // but schould not have the mail URL scheme
    if ([email hasPrefix:@"mailto:"])
    {
        return NO;
    }

    // no complaints, string is valid email address
    return YES;
}

- (BOOL)passwordPolicy:(NSString *)password
{
    return password.length > 3; // Todo make some rule we want
}


@end
