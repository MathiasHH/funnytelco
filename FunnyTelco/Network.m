//
//  Network.m
//  todo-ObjC
//
//  Created by Mathias Hansen on 23/02/16.
//  Copyright © 2016 Mathias Hedemann Hansen. All rights reserved.
//

#import "Network.h"
#import <AFNetworking/AFNetworking.h>
#import "MyMacros.h"

@implementation Network

-(void)login:(NSDictionary *)usernameandpassword withCallback:(void (^)(id, id))callback {

#ifdef FAKE_SERVER
	/* Debug only code */
	NSString *userName = usernameandpassword[@"username"];
	NSString *password = usernameandpassword[@"password"];
	if([userName isEqualToString:@"bob@mail.dk"] && [password isEqualToString:@"kodeord"])
		callback(nil, @{@"data" :
				@{@"authToken" : @"hiKUL6LLXyeSdLRzXI8HxR6972f6zm8Qjb2ioaxQ6Wi",
						@"userId" : @"DZekBStbiw8PWpSoi",
				},
				@"status" : @"success",
		});
	else
		callback(NSLocalizedString(@"Error :(", nil), nil);
#else
	AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
		//	NSDictionary *parameters = @{@"username":@"bob@mail.dk",@"password":@"kodeord"};

		[manager POST:@"http://localhost:3000/api/login"
		   parameters:usernameandpassword
			 progress:nil
			  success:^(NSURLSessionTask *task, id responseObject) {
				  NSLog(@"Success: %@", responseObject);
				  callback(nil, responseObject);
			  } failure:^(NSURLSessionTask *operation, NSError *error) {
					NSLog(@"Error: %@", error);
					callback(error.userInfo[@"NSLocalizedDescription"], nil);
				}];
#endif




}

@end
