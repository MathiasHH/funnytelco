//
//  Network.h
//  todo-ObjC
//
//  Created by Mathias Hansen on 23/02/16.
//  Copyright © 2016 Mathias Hedemann Hansen. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LoginViewController;

@interface Network : NSObject

-(void)login:(NSDictionary *)usernameandpassword withCallback:(void(^) (id, id))callback;

@end
