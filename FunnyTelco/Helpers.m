//
// Created by Mathias Hansen on 28/02/16.
// Copyright (c) 2016 Mathias Hedemann Hansen. All rights reserved.
//

#import "Helpers.h"
#import "LoginViewController.h"


@implementation Helpers

+ (void)gotoViewController:(NSString *)gotoVC fromViewController:(UIViewController *)fromVC block:(void (^)(void))block;
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *gotoViewController = [storyboard instantiateViewControllerWithIdentifier:gotoVC];
    [fromVC presentViewController:gotoViewController animated:YES completion:^{
        block();
    }];
}

+ (void)setBackgroundImageForView:(UIView *)view image:(NSString *)string {
    UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:string]];
    [view addSubview:backgroundView];
    [view sendSubviewToBack:backgroundView];
}

@end