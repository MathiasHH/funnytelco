For: App Transport Security has blocked a cleartext HTTP (http://) resource load since it is insecure. Temporary exceptions can be configured via your app's Info.plist file.
2016-02-25 13:19:14.034 todo-ObjC[9694:423089] Error: Error Domain=NSURLErrorDomain Code=-1022 "The resource could not be loaded because the App Transport Security policy requires the use of a secure connection." UserInfo={NSUnderlyingError=0x7fb02a4202f0 {Error Domain=kCFErrorDomainCFNetwork Code=-1022 "(null)"}, NSErrorFailingURLStringKey=http://localhost:3000/api/login, NSErrorFailingURLKey=http://localhost:3000/api/login, NSLocalizedDescription=The resource could not be loaded because the App Transport Security policy requires the use of a secure connection.}
Do:  <key>NSAppTransportSecurity</key>
      <dict>
        <!--See http://ste.vn/2015/06/10/configuring-app-transport-security-ios-9-osx-10-11/ -->
        <key>NSAllowsArbitraryLoads</key>
        <true/>
      </dict>
In Info.plist. Remember to remove this and setup https in a production app.







