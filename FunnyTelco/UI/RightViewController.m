//
//  RightViewController.m
//  FunnyTelco
//
//  Created by Mathias Hansen on 01/03/16.
//  Copyright © 2016 Mathias Hedemann Hansen. All rights reserved.
//

#import "RightViewController.h"
#import "Helpers.h"

@interface RightViewController ()
@end

@implementation RightViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor yellowColor];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section;
{
    return 80;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell =  [tableView cellForRowAtIndexPath:indexPath];
    if(indexPath.row == 10) [self logoutClick];

    NSString *text = [self getStringFromLabelInCell:cell withTag: 1 + indexPath.row];
    if(!text) text = @"";
    [self.delegate aCellHasBeenClicked:text];
}

- (NSString *)getStringFromLabelInCell:(UITableViewCell *)cell withTag:(NSInteger)tag {
    return  [(UILabel *)[cell.contentView viewWithTag:tag] text];
}

- (void)logoutClick;
{
    [Helpers gotoViewController:@"LoginViewController" fromViewController:self block:^{
        // logout. Now we just set this to false. In production we would call the server to logout to destroy the token.
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isLoggedIn"];
    }];
}

@end
