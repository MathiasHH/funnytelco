//
//  RightViewController.h
//  FunnyTelco
//
//  Created by Mathias Hansen on 01/03/16.
//  Copyright © 2016 Mathias Hedemann Hansen. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RightViewControllerDelegate <NSObject>
- (void)aCellHasBeenClicked:(id)data;
@end

/*
 * // TODO support multiple language for right menu. See the storyboard
 */

@interface RightViewController : UITableViewController
@property (nonatomic, assign) id<RightViewControllerDelegate> delegate;
@end
