//
//  CenterViewController.h
//  FunnyTelco
//
//  Created by Mathias Hansen on 01/03/16.
//  Copyright © 2016 Mathias Hedemann Hansen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RightViewController.h"

@protocol CenterViewControllerDelegate <NSObject>
- (void)movePanelToOriginalPosition;
- (void)movePanelLeft;
@end


@interface CenterViewController : UIViewController
@property (nonatomic, assign) id<CenterViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIButton *buttonRight;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@end
