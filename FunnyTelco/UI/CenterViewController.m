//
//  CenterViewController.m
//  FunnyTelco
//
//  Created by Mathias Hansen on 01/03/16.
//  Copyright © 2016 Mathias Hedemann Hansen. All rights reserved.
//

#import "CenterViewController.h"
#import "Helpers.h"

@interface CenterViewController ()
@end

@implementation CenterViewController

- (void)viewDidLoad;
{
	[super viewDidLoad];
}
#pragma mark Button Actions

- (IBAction)buttonMovePanelToTheLeft:(UIButton *)button {
	switch (button.tag) {
		case 0: {
			[self.delegate movePanelToOriginalPosition];
			break;
		}
		case 1: {
			[self.delegate movePanelLeft];
			break;
		}
		default:
			break;
	}
}

@end
