//
//  MainViewController.m
//  FunnyTelco
//
//  Created by Mathias Hansen on 01/03/16.
//  Copyright © 2016 Mathias Hedemann Hansen. All rights reserved.
//

#import "MainViewController.h"
#import "CenterViewController.h"
#import "RightViewController.h"

#define CENTER_TAG 1
#define RIGHT_PANEL_TAG 3
#define CORNER_RADIUS 4
#define SLIDE_TIMING .25
#define PANEL_WIDTH 60

@interface MainViewController () <CenterViewControllerDelegate, RightViewControllerDelegate, UIGestureRecognizerDelegate>
@property (nonatomic, strong) CenterViewController *centerViewController;
@property (nonatomic, strong) RightViewController *rightPanelViewController;
@property (nonatomic, assign) BOOL showingRightPanel;
@property (nonatomic, assign) BOOL showPanel;
@end

@implementation MainViewController
{
	UIStoryboard *_storyboard;
	CGFloat _widthOfScreen;
	int _lastOrientation;
}

#pragma mark View Did Load
- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	[self setupView];

	// we setup a notification for device change because we want to close the Right panel when we change orientation
	[[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceOrientationDidChange:) name:UIDeviceOrientationDidChangeNotification object:nil];
}


- (void)deviceOrientationDidChange:(id)deviceOrientationDidChange {
	UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];

	if(self.showingRightPanel) {
		if(		/* going from landscape to portrait*/
				(_lastOrientation == UIDeviceOrientationLandscapeRight && orientation == UIDeviceOrientationPortrait)
						/* or going from other landscape to portrait*/
						|| (_lastOrientation == UIDeviceOrientationLandscapeLeft && orientation == UIDeviceOrientationPortrait)
						/* or going from lanscape to upsidedown*/
						|| (_lastOrientation == UIDeviceOrientationLandscapeRight && orientation == UIDeviceOrientationPortraitUpsideDown)
						/* or going from other lanscape to upsidedown*/
						|| (_lastOrientation == UIDeviceOrientationLandscapeLeft && orientation == UIDeviceOrientationPortraitUpsideDown)
						/* or going from portrait to landscape*/
						|| (_lastOrientation == UIDeviceOrientationPortrait && orientation == UIDeviceOrientationLandscapeRight)
						/* or going from portrait to other landscape*/
						|| (_lastOrientation == UIDeviceOrientationPortrait && orientation == UIDeviceOrientationLandscapeLeft)
						/* or going from upsite down to landscape*/
						|| (_lastOrientation == UIDeviceOrientationPortraitUpsideDown && orientation ==
						UIDeviceOrientationLandscapeRight)
						/* or going from upsite down to other landscape*/
						|| (_lastOrientation == UIDeviceOrientationPortraitUpsideDown && orientation ==
						UIDeviceOrientationLandscapeLeft)
						/* going from UIDeviceOrientationFaceUp to landscape*/
						|| (_lastOrientation == UIDeviceOrientationFaceUp  && orientation ==
						UIDeviceOrientationLandscapeRight)
						/* going from UIDeviceOrientationFaceUp to other landscape */
						|| (_lastOrientation == UIDeviceOrientationFaceUp  && orientation ==
						UIDeviceOrientationLandscapeLeft)
						/* going from UIDeviceOrientationUnknown to UIDeviceOrientationLandscapeRight */
						|| (_lastOrientation == UIDeviceOrientationUnknown  && orientation ==
						UIDeviceOrientationLandscapeRight)
						/* Going from UIDeviceOrientationUnknown  to UIDeviceOrientationLandscapeRight */
						|| (_lastOrientation == UIDeviceOrientationUnknown  && orientation ==
						UIDeviceOrientationLandscapeLeft)
						/* Going from UIDeviceOrientationUnknown  to UIDeviceOrientationPortrait */
						|| (_lastOrientation == UIDeviceOrientationUnknown  && orientation ==
						UIDeviceOrientationPortrait)
				)
		{
			[self movePanelToOriginalPosition];
		}
	}

	_lastOrientation = orientation;

	_widthOfScreen = CGRectGetWidth(self.view.bounds);

}

- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}


#pragma mark Setup View
- (void)setupView;
{
	_storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
	// setup center view
	self.centerViewController = [_storyboard instantiateViewControllerWithIdentifier:@"CenterViewController"];
	self.centerViewController.view.tag = CENTER_TAG;
	self.centerViewController.delegate = self;

	[self.view addSubview:self.centerViewController.view];
	[self addChildViewController:self.centerViewController];

	[self.centerViewController didMoveToParentViewController:self];

	[self setupGestures];

}

- (void)showCenterViewWithShadow:(BOOL)value withOffset:(CGFloat)offset
{
	if (value)
	{
		[self.centerViewController.view.layer setCornerRadius:CORNER_RADIUS];
		[self.centerViewController.view.layer setShadowColor:[UIColor blackColor].CGColor];
		[self.centerViewController.view.layer setShadowOpacity:0.8];
		[self.centerViewController.view.layer setShadowOffset:CGSizeMake(offset, offset)];
	}
	else
	{
		[self.centerViewController.view.layer setCornerRadius:0.0f];
		[self.centerViewController.view.layer setShadowOffset:CGSizeMake(offset, offset)];
	}
}

- (void)resetMainView
{
	// remove right view and reset variables, if needed
	if (self.rightPanelViewController != nil)
	{
		[self.rightPanelViewController.view removeFromSuperview];
		self.rightPanelViewController = nil;

		self.centerViewController.buttonRight.tag = 1;
		self.showingRightPanel = NO;
	}

	// remove view shadows
	[self showCenterViewWithShadow:NO withOffset:0];
}

- (UIView *)getRightView
{
	// init view if it doesn't already exist
	if (self.rightPanelViewController == nil)
	{
		// this is where you define the view for the right panel
		self.rightPanelViewController = [_storyboard instantiateViewControllerWithIdentifier:@"RightViewController"];
		self.rightPanelViewController.delegate = self;
		self.rightPanelViewController.view.tag = RIGHT_PANEL_TAG;
		[self.view addSubview:self.rightPanelViewController.view];
		[self addChildViewController:self.rightPanelViewController];
		[self.rightPanelViewController didMoveToParentViewController:self];
		self.rightPanelViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
	}

	self.showingRightPanel = YES;

	// set up view shadows
	[self showCenterViewWithShadow:YES withOffset:2];

	UIView *view = self.rightPanelViewController.view;
	return view;
}

#pragma mark Swipe Gesture Setup And touches
- (void)setupGestures
{
	UIPanGestureRecognizer *panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(movePanel:)];
	[panRecognizer setMinimumNumberOfTouches:1];
	[panRecognizer setMaximumNumberOfTouches:1];
	[panRecognizer setDelegate:self];

	[self.centerViewController.view addGestureRecognizer:panRecognizer];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event;
{
	[super touchesBegan:touches withEvent:event];

	UITouch *touch = [[event allTouches] anyObject];
	CGPoint touchPoint = [touch locationInView:self.view];

	[self IfWeAlmostTouchHamburgerMenuOrTouchingLeftSideOfRightViewControllesViewStillOpenCloseView:touchPoint];
}

- (void)IfWeAlmostTouchHamburgerMenuOrTouchingLeftSideOfRightViewControllesViewStillOpenCloseView:(CGPoint)touchPoint
{
	CGFloat top = 0;
	UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
	top = UIInterfaceOrientationIsPortrait(interfaceOrientation) ? 60.0f : 40.0f;
	BOOL clickingOnTopRightSideOfCentreViewControllersView = touchPoint.y < top && touchPoint.x > _widthOfScreen -
			PANEL_WIDTH;
	BOOL clickingOnLeftSideOfRightViewControllersView = touchPoint.y < top && touchPoint.x < PANEL_WIDTH;

	if(clickingOnTopRightSideOfCentreViewControllersView){
		[self movePanelLeft];
	}
	else if(self.showingRightPanel && clickingOnLeftSideOfRightViewControllersView){
		[self movePanelToOriginalPosition];
	}
}


-(void)movePanel:(id)sender
{
	[[(UITapGestureRecognizer*)sender view].layer removeAllAnimations];

	CGPoint translatedPoint = [(UIPanGestureRecognizer*)sender translationInView:self.view];
	CGPoint velocity = [(UIPanGestureRecognizer*)sender velocityInView:[sender view]];

	if([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateBegan) {
		UIView *childView = nil;

		// if we swipe to the left
		if(velocity.x < 0)
			childView = [self getRightView];

		// Make sure the view you're working with is front and center.
		[self.view sendSubviewToBack:childView];
		[[sender view] bringSubviewToFront:[(UIPanGestureRecognizer*)sender view]];
	}

	if([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateEnded) {

		if (!self.showPanel) {
			[self movePanelToOriginalPosition];
		}
		else if (self.showingRightPanel) {
			[self movePanelLeft];
		}
	}

	if([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateChanged) {

		if(!self.showingRightPanel && !self.showPanel)
		{
			return; // we dont want to swipe from left to right at centerviewcontroller when we only have a
			// rightpanelviewcontroller and no leftpanelviewcontroller
		}

		// Are you more than halfway? If so, show the panel when done dragging by setting this value to YES (1).
		self.showPanel = abs((int) ([sender view].center.x - self.centerViewController.view.frame.size.width/2)) > self.centerViewController.view.frame.size.width/2;

		CGFloat x = [sender view].center.x + translatedPoint.x;
		// if translatedPoint.x is bigger than 0 we are dragging from left to right
		// we only want to drag the view till the view hits the edge of the screen and its only when we are dragging from left to right
		if(x > 160.0f && translatedPoint.x > 0) x = 160.0f; //return;

		// Allow dragging only in x-coordinates by only updating the x-coordinate with translation position.
		[sender view].center = CGPointMake(x, [sender view].center.y);
		[(UIPanGestureRecognizer*)sender setTranslation:CGPointMake(0, 0) inView:self.view];
	}
}


#pragma mark Delegate Actions

- (void)aCellHasBeenClicked:(id)data {
	if ([data isKindOfClass:[NSString class]])
		self.centerViewController.headerLabel.text = data;
	[self movePanelToOriginalPosition];
}

- (void)movePanelToOriginalPosition
{
	[UIView animateWithDuration:SLIDE_TIMING delay:0
						options:UIViewAnimationOptionBeginFromCurrentState
					 animations:^ {
						 self.centerViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
					 }
					 completion:^(BOOL finished) {
						 if (finished) {
							 [self resetMainView];
						 }
					 }];
}

- (void)movePanelLeft // to show right panel
{
	UIView *childView = [self getRightView];
	[self.view sendSubviewToBack:childView];

	[UIView animateWithDuration:SLIDE_TIMING delay:0 options:UIViewAnimationOptionBeginFromCurrentState
					 animations:^{
						 self.centerViewController.view.frame =
								 CGRectMake(-self.view.frame.size.width + PANEL_WIDTH, 0,
										 self.view.frame.size.width, self.view.frame.size.height);
					 }
					 completion:^(BOOL finished) {
						 if (finished) {
							 self.centerViewController.buttonRight.tag = 0;
						 }
					 }];
}

@end
