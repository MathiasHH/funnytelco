//
//  LoginViewController.m
//  todo-ObjC
//
//  Created by Mathias Hansen on 19/02/16.
//  Copyright © 2016 Mathias Hedemann Hansen. All rights reserved.
//

#import "LoginViewController.h"
#import "Network.h"
#import "Helpers.h"
#import "LoginRules.h"


@interface LoginViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *errorLabel;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *logInButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

@end

@implementation LoginViewController {
	BOOL _isLoading;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view.

	self.logInButton.layer.cornerRadius = 3;
	self.logInButton.clipsToBounds = YES;
	self.errorLabel.layer.cornerRadius = 3;
	self.errorLabel.clipsToBounds = YES;
	self.errorLabel.text = @"";

	self.emailTextField.delegate = self;
	self.passwordTextField.delegate = self;
	self.passwordTextField.secureTextEntry = YES;
}

-(void)errorMessage:(NSString *)message {
	self.errorLabel.text = message;
}



- (IBAction)login {
	[self.passwordTextField endEditing:YES];
	[self.emailTextField endEditing:YES];

	NSString *email = self.emailTextField.text;
	NSString *password = self.passwordTextField.text;
	LoginRules *loginRules = [LoginRules new];
	if( ![loginRules isValidEmail:email] || ![loginRules passwordPolicy:password]) {
		[self errorMessage:NSLocalizedString(@"Wrong Email or Password", nil)];
		return;
	}

	[self.spinner startAnimating];
	_isLoading = YES;
	NSDictionary *parameters = @{@"username": email,@"password":password};

	[[Network new] login:parameters withCallback:^(NSString *error, id responseObject) {
		[self.spinner stopAnimating];
		_isLoading = NO;
		if(error) {
			[self errorMessage:error];
		}else {
			NSString *userIdKey = @"userId";
			NSString *authTokenKey = @"authToken";
			id data = [responseObject objectForKey:@"data"] ;
			id userId = [data objectForKey:userIdKey];
			id authToken = [data objectForKey:authTokenKey];
			// todo maybe use realmio for for login functionality
			// we just do this now for simplicity
			[[NSUserDefaults standardUserDefaults] setObject:userId forKey:userIdKey];
			[[NSUserDefaults standardUserDefaults] setObject:authToken forKey:authTokenKey];
			[[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isLoggedIn"];
			[self performSegueWithIdentifier: @"showMainViewController" sender: nil];
		}
	}];
}

// if user is touching a textfield to start typing we dont want and error message
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {

	// if there is an error message
	if (self.errorLabel.text.length > 0) {
		self.errorLabel.text = @"";
	}

	return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
	// when clicking done on the keyboard after clicking on password we want to login
	if(textField.tag == 2) {
		[self login];
	}
	// hide keyboard when clicking done on the keyboard
	[textField resignFirstResponder];
	return NO;
}

// hide keyboard when clicking outside textfield
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
	[self.view endEditing:YES];
}



- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}


@end
